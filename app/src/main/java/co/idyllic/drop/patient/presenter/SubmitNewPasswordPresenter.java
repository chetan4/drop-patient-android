package co.idyllic.drop.patient.presenter;

import co.idyllic.drop.patient.model.pojo.RequestSuccess;
import co.idyllic.drop.patient.view.viewinterfaces.SubmitNewPasswordView;
import rx.Observer;

/**
 * Created by Vishal on 13-09-2017.
 */

public class SubmitNewPasswordPresenter extends BasePresenter implements Observer<RequestSuccess> {
    public SubmitNewPasswordView viewInterface;
    @Override
    public void onCompleted() {
        viewInterface.onPasswordSubmitComplete();
    }

    @Override
    public void onError(Throwable e) {
        viewInterface.onPasswordSubmitError(e.getMessage());
    }

    @Override
    public void onNext(RequestSuccess requestSuccess) {
        viewInterface.onPasswordSubmitSuccess(requestSuccess);
    }

    public void submitNewPassword(String password){
        unSubscribeAll();
        subscribe(viewInterface.submitPassword(password), SubmitNewPasswordPresenter.this);
    }
}
