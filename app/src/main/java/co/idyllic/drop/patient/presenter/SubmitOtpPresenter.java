package co.idyllic.drop.patient.presenter;

import co.idyllic.drop.patient.model.pojo.RequestSuccess;
import co.idyllic.drop.patient.view.viewinterfaces.SubmitOTPView;
import rx.Observer;

/**
 * Created by Vishal on 13-09-2017.
 */

public class SubmitOtpPresenter extends BasePresenter implements Observer<RequestSuccess> {
    public SubmitOTPView viewInterface;
    @Override
    public void onCompleted() {
        viewInterface.onOtpSubmitComplete();
    }

    @Override
    public void onError(Throwable e) {
        viewInterface.onOtpSubmitError(e.getMessage());
    }

    @Override
    public void onNext(RequestSuccess requestSuccess) {
        viewInterface.onOtpSubmitSuccess(requestSuccess);
    }

    public void submitOtp(String otp){
        unSubscribeAll();
        subscribe(viewInterface.submitOtp(otp), SubmitOtpPresenter.this);
    }
}
