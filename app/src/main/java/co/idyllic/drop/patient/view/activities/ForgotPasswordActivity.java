package co.idyllic.drop.patient.view.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.idyllic.drop.patient.R;
import co.idyllic.drop.patient.dagger.DaggerInjecter;
import co.idyllic.drop.patient.model.api.NewPasswordService;
import co.idyllic.drop.patient.model.api.OtpService;
import co.idyllic.drop.patient.model.api.PhoneNumberService;
import co.idyllic.drop.patient.model.pojo.RequestSuccess;
import co.idyllic.drop.patient.presenter.SubmitNewPasswordPresenter;
import co.idyllic.drop.patient.presenter.SubmitOtpPresenter;
import co.idyllic.drop.patient.presenter.SubmitPhoneNumberPresenter;
import co.idyllic.drop.patient.view.viewinterfaces.SubmitNewPasswordView;
import co.idyllic.drop.patient.view.viewinterfaces.SubmitOTPView;
import co.idyllic.drop.patient.view.viewinterfaces.SubmitPhoneNumberView;
import co.idyllic.drop.patient.view.widgets.NonSwipeableViewPager;
import io.paperdb.Paper;
import rx.Observable;

public class ForgotPasswordActivity extends BaseActivity implements SubmitPhoneNumberView,
        SubmitOTPView, SubmitNewPasswordView {

    List<View> viewList = new ArrayList<>();
    @BindView(R.id.viewPager)
    NonSwipeableViewPager viewPager;
    Button submitPhoneNumber, submitOtp, submitNewPassword;
    SubmitPhoneNumberPresenter mPhoneNumberPresenter;
    SubmitNewPasswordPresenter mSubmitNewPasswordPresenter;
    SubmitOtpPresenter mOtpPresenter;
    EditText mPhoneNumberEditText, mOtpEditText, mNewPasswordEditText, mConfirmPasswordEditText;
    String mPhoneNumber = "";
    @Inject
    PhoneNumberService mPhoneNumberApi;
    @Inject
    OtpService mOtpService;
    @Inject
    NewPasswordService mNewPasswordService;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);
        DaggerInjecter.getApiComponent().inject(this);
        mPhoneNumberPresenter = new SubmitPhoneNumberPresenter();
        mPhoneNumberPresenter.viewInterface = ForgotPasswordActivity.this;
        mOtpPresenter = new SubmitOtpPresenter();
        mOtpPresenter.viewInterface = ForgotPasswordActivity.this;
        mSubmitNewPasswordPresenter = new SubmitNewPasswordPresenter();
        mSubmitNewPasswordPresenter.viewInterface = ForgotPasswordActivity.this;
        initViews();
    }

    private void initViews() {
        LayoutInflater inflater = LayoutInflater.from(this);
        viewList = new ArrayList<View>();
        for (int i = 0; i < 3; i++) {
            View view = null;
            if(i ==0){
                view = inflater.inflate(R.layout.layout_forgotpwd_phone, null);
                submitPhoneNumber = view.findViewById(R.id.submit_btn);
                mPhoneNumberEditText = view.findViewById(R.id.phone_editText);
            } else if(i ==1){
                view = inflater.inflate(R.layout.layout_forgotpwd_otp, null);
                submitOtp = view.findViewById(R.id.submit_btn);
                mOtpEditText = view.findViewById(R.id.otp_editText);
            } else if(i ==2){
                view = inflater.inflate(R.layout.layout_forgotpwd_newpwd, null);
                submitNewPassword = view.findViewById(R.id.submit_btn);
                mNewPasswordEditText = view.findViewById(R.id.new_pwd_editText);
                mConfirmPasswordEditText = view.findViewById(R.id.confirm_pwd_editText);
            }
            if(view != null)
                viewList.add(view);
        }
        PagerAdapter pagerAdapter = new PagerAdapter() {
            @Override
            public boolean isViewFromObject(View arg0, Object arg1) {
                return arg0 == arg1;
            }

            @Override
            public int getCount() {
                return viewList.size();
            }

            @Override
            public void destroyItem(ViewGroup container, int position,
                                    Object object) {
                container.removeView(viewList.get(position));
            }

            @Override
            public int getItemPosition(Object object) {
                return super.getItemPosition(object);
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return "title";
            }

            @Override
            public Object instantiateItem(ViewGroup container, int position) {
                container.addView(viewList.get(position));
                return viewList.get(position);
            }
        };
        viewPager.setAdapter(pagerAdapter);
        submitPhoneNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPhoneNumber = mPhoneNumberEditText.getText().toString();
                if(!mPhoneNumber.equals("")){
                    mPhoneNumberPresenter.submitPhoneNumber(mPhoneNumber);
                } else {
                    showToast("Phone Number cannot be empty");
                }
            }
        });
        submitOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String otp = mOtpEditText.getText().toString();
                if(!otp.equals("")) {
                    mOtpPresenter.submitOtp(otp);
                } else {
                    showToast("OTP cannot be empty");
                }
            }
        });
        submitNewPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String newPwd = mNewPasswordEditText.getText().toString();
                String confirmPwd = mConfirmPasswordEditText.getText().toString();
                if(newPwd.equals(confirmPwd)){
                    mSubmitNewPasswordPresenter.submitNewPassword(confirmPwd);
                } else {
                    showToast("Passwords doesn't match");
                }
            }
        });
    }

    @Override
    public void onPhoneNumberSubmitComplete() {

    }

    @Override
    public void onPhoneNumberSubmitError(String message) {
        showToast("Could not reach servers");
    }

    @Override
    public void onPhoneNumberSubmitSuccess(RequestSuccess result) {
        if(result.getSuccess()){
            viewPager.setCurrentItem(1);
        }
    }

    @Override
    public Observable<RequestSuccess> submitPhoneNumber(String phone) {
        return mPhoneNumberApi.submitPhoneNumber(phone);
    }

    @Override
    public void onOtpSubmitComplete() {

    }

    @Override
    public void onOtpSubmitError(String message) {
        showToast("Could not reach servers");
    }

    @Override
    public void onOtpSubmitSuccess(RequestSuccess result) {
        if(result.getSuccess()){
            viewPager.setCurrentItem(2);
        } else{
            showToast("Invalid OTP");
        }
    }

    @Override
    public Observable<RequestSuccess> submitOtp(String otp) {
        return mOtpService.submitOtp(otp);
    }

    @Override
    public void onPasswordSubmitComplete() {

    }

    @Override
    public void onPasswordSubmitError(String message) {
        showToast("Could not reach servers");
    }

    @Override
    public void onPasswordSubmitSuccess(RequestSuccess result) {
        if(result.getSuccess()){
            showToast("Passwors successfully changed");
        } else {
            showToast("Password not changed");
        }
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        }, 200);
    }

    @Override
    public Observable<RequestSuccess> submitPassword(String otp) {
        return mNewPasswordService.resetPassword(mPhoneNumber, otp);
    }
}
