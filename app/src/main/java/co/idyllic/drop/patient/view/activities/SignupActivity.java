package co.idyllic.drop.patient.view.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.idyllic.drop.patient.R;
import co.idyllic.drop.patient.dagger.DaggerInjecter;
import co.idyllic.drop.patient.model.api.SignupService;
import co.idyllic.drop.patient.model.pojo.ProfileRequest;
import co.idyllic.drop.patient.model.pojo.RequestSuccess;
import co.idyllic.drop.patient.model.pojo.User;
import co.idyllic.drop.patient.presenter.SignupPresenter;
import co.idyllic.drop.patient.view.viewinterfaces.SignupView;
import io.paperdb.Paper;
import retrofit2.Response;
import rx.Observable;

public class SignupActivity extends BaseActivity implements SignupView {
    Context mContext;
    @BindView(R.id.fullname_edittext)
    EditText fullNameEditText;
    @BindView(R.id.phone_edittext)
    EditText phoneNumberEditText;
    @BindView(R.id.email_edittext)
    EditText emailEditText;
    @BindView(R.id.password_edittext)
    EditText passwordEditText;
    @BindView(R.id.signup_button)
    Button signupButton;
    @BindView(R.id.signin_text)
    TextView signinTextView;
    @Inject
    SignupService mSignupApi;
    SignupPresenter mSignupPresenter;
    ProfileRequest signup;
    User mUser;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        mContext = this;
        ButterKnife.bind(this);
        DaggerInjecter.getApiComponent().inject(this);
        mSignupPresenter = new SignupPresenter();
        mSignupPresenter.signupView = this;
        Paper.init(mContext);
        init();
    }

    private void init() {
        signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signup = new ProfileRequest(fullNameEditText.getText().toString(),
                        phoneNumberEditText.getText().toString(), emailEditText.getText().toString(),
                        passwordEditText.getText().toString());
                mUser = new User(signup);
                mSignupPresenter.signup(mUser);
            }
        });
        signinTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, LandingActivity.class);
                intent.putExtra("signin", true);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onSignupComplete() {

    }

    @Override
    public void onSignupError(String message) {
        showToast("Could not reach servers");
    }



    @Override
    public void onSignupSuccess(Response<RequestSuccess> result) {
        Paper.book().write("user", mUser);
        Intent intent = new Intent(mContext, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public Observable<Response<RequestSuccess>> signup(ProfileRequest signup) {
        return mSignupApi.signup(signup);
    }
}
