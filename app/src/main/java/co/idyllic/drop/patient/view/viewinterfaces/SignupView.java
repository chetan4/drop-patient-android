package co.idyllic.drop.patient.view.viewinterfaces;

import co.idyllic.drop.patient.model.pojo.ProfileRequest;
import co.idyllic.drop.patient.model.pojo.RequestSuccess;
import retrofit2.Response;
import rx.Observable;

/**
 * Created by Vishal on 11-09-2017.
 */

public interface SignupView {
    void onSignupComplete();
    void onSignupError(String message);
    void onSignupSuccess(Response<RequestSuccess> result);
    Observable<Response<RequestSuccess>> signup(ProfileRequest signup);
}
