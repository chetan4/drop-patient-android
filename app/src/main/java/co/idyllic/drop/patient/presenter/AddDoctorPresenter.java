package co.idyllic.drop.patient.presenter;

import co.idyllic.drop.patient.model.pojo.AddDoctorRequestBody;
import co.idyllic.drop.patient.model.pojo.AddDoctorResponse;
import co.idyllic.drop.patient.model.pojo.RequestSuccess;
import co.idyllic.drop.patient.view.viewinterfaces.AddDoctorView;
import rx.Observer;

/**
 * Created by Vishal on 14-09-2017.
 */

public class AddDoctorPresenter extends BasePresenter implements Observer<AddDoctorResponse> {
    public AddDoctorView viewInterface;
    @Override
    public void onCompleted() {
        viewInterface.onAddComplete();
    }

    @Override
    public void onError(Throwable e) {
        viewInterface.onAddError(e.getMessage());
    }

    @Override
    public void onNext(AddDoctorResponse requestSuccess) {
        viewInterface.onAddSuccess(requestSuccess);
    }

    public void addDoctor(AddDoctorRequestBody body){
        unSubscribeAll();
        subscribe(viewInterface.addDoctor(body), AddDoctorPresenter.this);
    }
}
