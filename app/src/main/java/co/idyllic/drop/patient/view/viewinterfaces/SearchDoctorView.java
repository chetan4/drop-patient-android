package co.idyllic.drop.patient.view.viewinterfaces;

import co.idyllic.drop.patient.model.pojo.SearchDoctorResponse;
import rx.Observable;

/**
 * Created by Vishal on 15-09-2017.
 */

public interface SearchDoctorView {
    void onSearchComplete();
    void onSearchError(String message);
    void onSearchSuccess(SearchDoctorResponse result);
    Observable<SearchDoctorResponse> search(String code);
}
