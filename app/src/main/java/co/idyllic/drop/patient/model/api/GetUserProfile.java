package co.idyllic.drop.patient.model.api;

import co.idyllic.drop.patient.model.pojo.ProfileResponse;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Vishal on 19-09-2017.
 */

public interface GetUserProfile {
    @GET("/api/patients/v1/profiles")
    Observable<Response<ProfileResponse>> getUserProfile(@Header("Authorization") String token);
}
