package co.idyllic.drop.patient.dagger.modules;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import co.idyllic.drop.patient.Constants;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Vishal on 08-09-2017.
 */
@Module
public class NetworkModule {
    private String mBaseUrl;
    public Context mContext;
    public NetworkModule(String baseUrl){
        mBaseUrl = baseUrl;
    }
    @Provides
    @Singleton
    RxJavaCallAdapterFactory providesRxJavaCallAdapterFactory(){
        return RxJavaCallAdapterFactory.create();
    }
    @Provides
    @Singleton
    GsonConverterFactory providesGsonConverterFactory(){
        return GsonConverterFactory.create();
    }

    @Provides
    @Singleton
    OkHttpClient providesOkHttpClient(){
        return new OkHttpClient.Builder()
                .readTimeout(20, TimeUnit.SECONDS)
                .connectTimeout(20, TimeUnit.SECONDS)
                .build();
    }

    @Provides
    @Singleton
    Retrofit providesRetrofit(OkHttpClient okHttpClient, GsonConverterFactory gsonConverterFactory,
                              RxJavaCallAdapterFactory rxJavaCallAdapterFactory){
        return new Retrofit.Builder()
                .addConverterFactory(gsonConverterFactory)
                .addCallAdapterFactory(rxJavaCallAdapterFactory)
                .client(okHttpClient)
                .baseUrl(mBaseUrl).build();
    }

    @Provides
    @Singleton
    Context providesContext(){
        return mContext;
    }

    @Provides
    @Singleton
    SharedPreferences providesSharedPreferences(){
        SharedPreferences prefs = mContext.getSharedPreferences(Constants.PREFERENCES,
                Context.MODE_PRIVATE);
        return prefs;
    }
}
