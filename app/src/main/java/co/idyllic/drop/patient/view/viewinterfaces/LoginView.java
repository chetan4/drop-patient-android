package co.idyllic.drop.patient.view.viewinterfaces;

import com.google.gson.JsonObject;

import co.idyllic.drop.patient.model.pojo.Login;
import co.idyllic.drop.patient.model.pojo.LoginResponse;
import rx.Observable;

/**
 * Created by Vishal on 08-09-2017.
 */

public interface LoginView {
    void onLoginComplete();
    void onLoginError(String message);
    void onLoginSuccess(LoginResponse result);
    Observable<LoginResponse> login(Login login);
}
