package co.idyllic.drop.patient.model.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Vishal on 15-09-2017.
 */

public class AddDoctorRequestBody {

    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("device_type")
    @Expose
    private String deviceType;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

}
