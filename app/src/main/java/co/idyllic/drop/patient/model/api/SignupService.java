package co.idyllic.drop.patient.model.api;

import co.idyllic.drop.patient.model.pojo.ProfileRequest;
import co.idyllic.drop.patient.model.pojo.RequestSuccess;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by Vishal on 08-09-2017.
 */

public interface SignupService {
    @POST("/register")
    Observable<Response<RequestSuccess>> signup(@Body ProfileRequest signup);
}
