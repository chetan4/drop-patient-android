package co.idyllic.drop.patient.view.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.idyllic.drop.patient.Constants;
import co.idyllic.drop.patient.R;
import co.idyllic.drop.patient.dagger.DaggerInjecter;
import co.idyllic.drop.patient.model.api.GetUserProfile;
import co.idyllic.drop.patient.model.pojo.ProfileResponse;
import co.idyllic.drop.patient.model.pojo.UserProfile;
import co.idyllic.drop.patient.presenter.UserProfilePresenter;
import co.idyllic.drop.patient.view.viewinterfaces.UserProfileView;
import co.idyllic.drop.patient.view.widgets.NonSwipeableViewPager;
import io.paperdb.Paper;
import retrofit2.Response;
import rx.Observable;

public class MainActivity extends BaseActivity implements UserProfileView{

    TextView nameTextView;
    Button addDoctorButton;
    @BindView(R.id.chats_btn)
    Button chatsButton;
    @BindView(R.id.transactions_btn)
    Button transactionsBtn;
    @BindView(R.id.profile_btn)
    Button profileButton;
    @BindView(R.id.viewPager)
    NonSwipeableViewPager viewPager;
    TextView profileNameTextView, profileEmailTextView, profilePhoneTextView, profileSignoutTextView;
    Button editProfileButton;
    Context mContext;
    List<View> viewList = new ArrayList<>();
    @Inject
    GetUserProfile mGetUserProfileApi;
    UserProfilePresenter mProfilePresenter;
    UserProfile mUser;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;
        ButterKnife.bind(this);
        DaggerInjecter.getApiComponent().inject(this);
        Paper.init(this);
        initViews();
        mProfilePresenter = new UserProfilePresenter();
        mProfilePresenter.viewInterface = MainActivity.this;
        mUser = Paper.book().read("user_profile");
        if(mUser == null){
            String authToken = Paper.book().read(Constants.KEY_AUTH_TOKEN);
            mProfilePresenter.getProfile(authToken);
        } else{
            populateData();
        }

    }

    private void populateData() {
        nameTextView.setText("Hi "+mUser.getFirstName());
        profileNameTextView.setText(mUser.getName());
        profileEmailTextView.setText(mUser.getEmail());
        profilePhoneTextView.setText(mUser.getPhoneNumber());
        nameTextView.setText(mUser.getName());

    }

    private void initViews() {

        LayoutInflater inflater = LayoutInflater.from(this);
        viewList = new ArrayList<View>();
        for (int i = 0; i < 3; i++) {
            View view = null;
            if(i ==0){
                view = inflater.inflate(R.layout.layout_chats, null);
                addDoctorButton = view.findViewById(R.id.add_doctor_btn);
                nameTextView = view.findViewById(R.id.name);
                addDoctorButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(mContext, SearchDoctorActivity.class);
                        startActivity(intent);
                    }
                });
            } else if(i ==1){
                view = inflater.inflate(R.layout.layout_transactions, null);
            } else if(i ==2){
                view = inflater.inflate(R.layout.layout_my_profile, null);
                profileNameTextView = view.findViewById(R.id.name_textView);
                profileEmailTextView = view.findViewById(R.id.email_textView);
                profilePhoneTextView = view.findViewById(R.id.phone_textView);
                profileSignoutTextView = view.findViewById(R.id.signout_textView);
                editProfileButton = view.findViewById(R.id.edit_profile_btn);
                editProfileButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(mContext, EditProfileActivity.class);
                        startActivity(intent);
                    }
                });
            }
            if(view != null)
                viewList.add(view);
        }
        PagerAdapter pagerAdapter = new PagerAdapter() {
            @Override
            public boolean isViewFromObject(View arg0, Object arg1) {
                return arg0 == arg1;
            }

            @Override
            public int getCount() {
                return viewList.size();
            }

            @Override
            public void destroyItem(ViewGroup container, int position,
                                    Object object) {
                container.removeView(viewList.get(position));
            }

            @Override
            public int getItemPosition(Object object) {
                return super.getItemPosition(object);
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return "title";
            }

            @Override
            public Object instantiateItem(ViewGroup container, int position) {
                container.addView(viewList.get(position));
                return viewList.get(position);
            }
        };
        viewPager.setAdapter(pagerAdapter);
        chatsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPager.setCurrentItem(0);
            }
        });
        transactionsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPager.setCurrentItem(1);
            }
        });
        profileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPager.setCurrentItem(2);
            }
        });
    }

    @Override
    public void onGetProfileComplete() {
        System.out.print("");
    }

    @Override
    public void onGetProfileErrorError(String message) {
        System.out.print("");
    }

    @Override
    public void onGetProfileSuccess(Response<ProfileResponse> result) {
        mUser = result.body().getData();
        Paper.book().write(Constants.KEY_USER_PROFILE, mUser);
        populateData();
    }

    @Override
    public Observable<Response<ProfileResponse>> getProfile(String authToken) {
        return mGetUserProfileApi.getUserProfile(authToken);
    }
}
