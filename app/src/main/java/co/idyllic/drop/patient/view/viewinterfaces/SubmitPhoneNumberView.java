package co.idyllic.drop.patient.view.viewinterfaces;

import co.idyllic.drop.patient.model.pojo.RequestSuccess;
import rx.Observable;

/**
 * Created by Vishal on 13-09-2017.
 */

public interface SubmitPhoneNumberView {
    void onPhoneNumberSubmitComplete();
    void onPhoneNumberSubmitError(String message);
    void onPhoneNumberSubmitSuccess(RequestSuccess result);
    Observable<RequestSuccess> submitPhoneNumber(String phone);
}
