package co.idyllic.drop.patient.presenter;

import co.idyllic.drop.patient.model.pojo.RequestSuccess;
import co.idyllic.drop.patient.model.pojo.ProfileRequest;
import co.idyllic.drop.patient.view.viewinterfaces.SignupView;
import retrofit2.Response;
import rx.Observer;

/**
 * Created by Vishal on 11-09-2017.
 */

public class SignupPresenter extends BasePresenter implements Observer<Response<RequestSuccess>> {
    public SignupView signupView;
    @Override
    public void onCompleted() {
        signupView.onSignupComplete();
    }

    @Override
    public void onError(Throwable e) {
        signupView.onSignupError(e.getMessage());
    }

    @Override
    public void onNext(Response<RequestSuccess> requestSuccess) {
        signupView.onSignupSuccess(requestSuccess);
    }
    public void signup(ProfileRequest signup){
        unSubscribeAll();
        subscribe(signupView.signup(signup), SignupPresenter.this);
    }
}
