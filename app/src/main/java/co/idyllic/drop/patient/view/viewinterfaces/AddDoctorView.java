package co.idyllic.drop.patient.view.viewinterfaces;

import co.idyllic.drop.patient.model.pojo.AddDoctorRequestBody;
import co.idyllic.drop.patient.model.pojo.AddDoctorResponse;
import co.idyllic.drop.patient.model.pojo.SearchDoctorRequestBody;
import co.idyllic.drop.patient.model.pojo.SearchDoctorResponse;
import rx.Observable;

/**
 * Created by Vishal on 15-09-2017.
 */

public interface AddDoctorView {
    void onAddComplete();
    void onAddError(String message);
    void onAddSuccess(AddDoctorResponse result);
    Observable<AddDoctorResponse> addDoctor(AddDoctorRequestBody search);
}
