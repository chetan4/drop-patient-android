package co.idyllic.drop.patient.model.api;

import co.idyllic.drop.patient.model.pojo.RequestSuccess;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Vishal on 13-09-2017.
 */

public interface NewPasswordService {
    @PUT("/reset_patient_password")
    Observable<RequestSuccess> resetPassword(@Query("phone_number") String phone,
                                         @Query("password") String password);
}
