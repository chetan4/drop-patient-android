package co.idyllic.drop.patient.view.viewinterfaces;

import co.idyllic.drop.patient.model.pojo.RequestSuccess;
import rx.Observable;

/**
 * Created by Vishal on 13-09-2017.
 */

public interface SubmitNewPasswordView {
    void onPasswordSubmitComplete();
    void onPasswordSubmitError(String message);
    void onPasswordSubmitSuccess(RequestSuccess result);
    Observable<RequestSuccess> submitPassword(String otp);
}
