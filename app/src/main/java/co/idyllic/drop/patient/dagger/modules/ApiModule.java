package co.idyllic.drop.patient.dagger.modules;

import javax.inject.Singleton;

import co.idyllic.drop.patient.dagger.CustomScope;
import co.idyllic.drop.patient.model.RxApiHelper;
import co.idyllic.drop.patient.model.api.AddDoctorService;
import co.idyllic.drop.patient.model.api.EditProfile;
import co.idyllic.drop.patient.model.api.GetUserProfile;
import co.idyllic.drop.patient.model.api.LoginService;
import co.idyllic.drop.patient.model.api.NewPasswordService;
import co.idyllic.drop.patient.model.api.OtpService;
import co.idyllic.drop.patient.model.api.PhoneNumberService;
import co.idyllic.drop.patient.model.api.SearchDoctorService;
import co.idyllic.drop.patient.model.api.SignupService;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by Vishal on 08-09-2017.
 */

@Module
public class ApiModule {

    @Provides
    @CustomScope
    RxApiHelper providesService(Retrofit retrofit){
        return retrofit.create(RxApiHelper.class);
    }

    @Provides
    @CustomScope
    LoginService providesLoginService(Retrofit retrofit){
        return retrofit.create(LoginService.class);
    }

    @Provides
    @CustomScope
    SignupService providesSignupService(Retrofit retrofit){
        return retrofit.create(SignupService.class);
    }

    @Provides
    @CustomScope
    PhoneNumberService providesPhoneNumberService(Retrofit retrofit){
        return retrofit.create(PhoneNumberService.class);
    }

    @Provides
    @CustomScope
    OtpService providesOtpService(Retrofit retrofit){
        return retrofit.create(OtpService.class);
    }

    @Provides
    @CustomScope
    NewPasswordService providesNewPasswordService(Retrofit retrofit){
        return retrofit.create(NewPasswordService.class);
    }

    @Provides
    @CustomScope
    SearchDoctorService providesSearchDoctorService(Retrofit retrofit){
        return retrofit.create(SearchDoctorService.class);
    }

    @Provides
    @CustomScope
    AddDoctorService providesAddDoctorService(Retrofit retrofit){
        return retrofit.create(AddDoctorService.class);
    }
    @Provides
    @CustomScope
    GetUserProfile providesHetUserProfile(Retrofit retrofit){
        return retrofit.create(GetUserProfile.class);
    }
    @Provides
    @CustomScope
    EditProfile providesEditProfile(Retrofit retrofit){
        return retrofit.create(EditProfile.class);
    }

}
