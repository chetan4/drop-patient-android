package co.idyllic.drop.patient.presenter;

/**
 * Created by Dell 3450 on 10/7/2016.
 */
public interface Presenter {

    void onCreate();

    void onPause();

    void onResume();

    void onDestroy();
}
