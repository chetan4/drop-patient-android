package co.idyllic.drop.patient.model.api;

import co.idyllic.drop.patient.model.pojo.SearchDoctorRequestBody;
import co.idyllic.drop.patient.model.pojo.SearchDoctorResponse;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Vishal on 15-09-2017.
 */

public interface SearchDoctorService {
    @GET("/api/patients/v1/profiles/search_doctor")
    Observable<SearchDoctorResponse> searchDoctor(@Header("Authorization") String token,
                                                  @Query("username") String doctorCode);
}
