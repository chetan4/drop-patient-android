package co.idyllic.drop.patient.view.viewinterfaces;

import co.idyllic.drop.patient.model.pojo.EditProfileRequest;
import co.idyllic.drop.patient.model.pojo.Login;
import co.idyllic.drop.patient.model.pojo.LoginResponse;
import co.idyllic.drop.patient.model.pojo.ProfileRequest;
import co.idyllic.drop.patient.model.pojo.ProfileResponse;
import co.idyllic.drop.patient.model.pojo.RequestSuccess;
import rx.Observable;

/**
 * Created by Vishal on 20-09-2017.
 */

public interface EditProfileView {
    void onEditComplete();
    void onEditError(String message);
    void onEditSuccess(ProfileResponse result);
    Observable<ProfileResponse> editProfile(EditProfileRequest editProfile);
}
