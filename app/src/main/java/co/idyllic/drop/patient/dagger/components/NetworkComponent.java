package co.idyllic.drop.patient.dagger.components;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Singleton;

import co.idyllic.drop.patient.dagger.modules.NetworkModule;
import dagger.Component;
import retrofit2.Retrofit;

/**
 * Created by Vishal on 08-09-2017.
 */
@Singleton
@Component(modules = NetworkModule.class)
public interface NetworkComponent {
    Retrofit retrofit();
    Context context();
    SharedPreferences sharedPreferences();
}
