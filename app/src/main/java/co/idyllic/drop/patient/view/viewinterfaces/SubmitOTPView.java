package co.idyllic.drop.patient.view.viewinterfaces;

import co.idyllic.drop.patient.model.pojo.RequestSuccess;
import rx.Observable;

/**
 * Created by Vishal on 13-09-2017.
 */

public interface SubmitOTPView {
    void onOtpSubmitComplete();
    void onOtpSubmitError(String message);
    void onOtpSubmitSuccess(RequestSuccess result);
    Observable<RequestSuccess> submitOtp(String otp);
}
