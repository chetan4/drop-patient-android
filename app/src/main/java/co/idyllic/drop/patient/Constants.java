package co.idyllic.drop.patient;

/**
 * Created by Vishal on 08-09-2017.
 */

public class Constants {
    public static final String PREFERENCES = "DROP_PATIENT";
    public static final String BASE_URL = "http://54.83.143.201";
    public static final String KEY_AUTH_TOKEN = "auth_token";
    public static final String KEY_USER_PROFILE = "user_profile";
}
