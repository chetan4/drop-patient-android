package co.idyllic.drop.patient.view.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.Gson;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.idyllic.drop.patient.Constants;
import co.idyllic.drop.patient.R;
import co.idyllic.drop.patient.dagger.DaggerInjecter;
import co.idyllic.drop.patient.model.api.SearchDoctorService;
import co.idyllic.drop.patient.model.pojo.RequestSuccess;
import co.idyllic.drop.patient.model.pojo.SearchDoctorRequestBody;
import co.idyllic.drop.patient.model.pojo.SearchDoctorResponse;
import co.idyllic.drop.patient.presenter.SearchDoctorPresenter;
import co.idyllic.drop.patient.view.viewinterfaces.SearchDoctorView;
import io.paperdb.Paper;
import retrofit2.Response;
import rx.Observable;

public class SearchDoctorActivity extends BaseActivity implements SearchDoctorView {
    @BindView(R.id.doctor_code_editText)
    EditText doctorCodeEditText;
    @BindView(R.id.next_btn)
    Button nextButton;
    Context mContext;
    String doctorCode;
    @Inject
    SearchDoctorService mSearchApi;
    SearchDoctorPresenter mSearchPresenter;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_doctor);
        mContext = this;
        ButterKnife.bind(this);
        DaggerInjecter.getApiComponent().inject(this);
        mSearchPresenter = new SearchDoctorPresenter();
        mSearchPresenter.viewInterface = SearchDoctorActivity.this;
        initViews();
    }

    private void initViews() {
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doctorCode = doctorCodeEditText.getText().toString();
                if(!doctorCode.equals("")) {
                    SearchDoctorRequestBody request = new SearchDoctorRequestBody();
                    request.setUsername(doctorCode);
                    mSearchPresenter.search(doctorCode);
                } else {
                    showToast("Please enter 4 digit code");
                }
            }
        });
    }

    @Override
    public void onSearchComplete() {

    }

    @Override
    public void onSearchError(String message) {
        showToast("Could not reach servers");
    }

    @Override
    public void onSearchSuccess(SearchDoctorResponse result) {
        if(result.getSuccess() && result.getData() != null) {
            Intent intent = new Intent(mContext, AddDoctorActivity.class);
            intent.putExtra("doctor_code", doctorCode);
            intent.putExtra("doctor", new Gson().toJson(result.getData()));
            startActivity(intent);
        } else{
            showToast("Invalid code");
        }
    }

    @Override
    public Observable<SearchDoctorResponse> search(String search) {
        String authToken = Paper.book().read(Constants.KEY_AUTH_TOKEN);
        return mSearchApi.searchDoctor(authToken, search);
    }
}
