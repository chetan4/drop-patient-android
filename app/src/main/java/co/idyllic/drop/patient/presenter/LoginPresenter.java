package co.idyllic.drop.patient.presenter;

import com.google.gson.JsonObject;

import co.idyllic.drop.patient.model.pojo.Login;
import co.idyllic.drop.patient.model.pojo.LoginResponse;
import co.idyllic.drop.patient.view.viewinterfaces.LoginView;
import rx.Observer;

/**
 * Created by Vishal on 08-09-2017.
 */

public class LoginPresenter extends BasePresenter implements Observer<LoginResponse> {
    public LoginView loginView;
    @Override
    public void onCompleted() {
        loginView.onLoginComplete();
    }

    @Override
    public void onError(Throwable e) {
        loginView.onLoginError(e.getMessage());
    }

    @Override
    public void onNext(LoginResponse loginResponse) {
        loginView.onLoginSuccess(loginResponse);
    }

    public void login(Login login){
        unSubscribeAll();
        subscribe(loginView.login(login), LoginPresenter.this);
    }
}
