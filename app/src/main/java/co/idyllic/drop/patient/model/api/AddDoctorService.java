package co.idyllic.drop.patient.model.api;

import co.idyllic.drop.patient.model.pojo.AddDoctorRequestBody;
import co.idyllic.drop.patient.model.pojo.AddDoctorResponse;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by Vishal on 14-09-2017.
 */

public interface AddDoctorService {
    @POST("/api/patients/v1/profiles/add_doctor")
    Observable<AddDoctorResponse> addDoctor(@Header("Authorization") String token,
                                            @Body AddDoctorRequestBody body);
}
