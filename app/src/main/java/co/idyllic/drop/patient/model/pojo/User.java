package co.idyllic.drop.patient.model.pojo;

/**
 * Created by Vishal on 13-09-2017.
 */

public class User extends ProfileRequest {
    public User(String fullName, String phone, String email, String password) {
        super(fullName, phone, email, password);
    }

    public User(ProfileRequest signup) {
        super(signup.getPatient().getName(), signup.getPatient().getPhoneNumber(),
                signup.getPatient().getEmail(), signup.getPatient().getPassword());
    }
}
