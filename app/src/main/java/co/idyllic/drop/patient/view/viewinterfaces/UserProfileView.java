package co.idyllic.drop.patient.view.viewinterfaces;

import co.idyllic.drop.patient.model.pojo.ProfileResponse;
import retrofit2.Response;
import rx.Observable;

/**
 * Created by Vishal on 19-09-2017.
 */

public interface UserProfileView {
    void onGetProfileComplete();
    void onGetProfileErrorError(String message);
    void onGetProfileSuccess(Response<ProfileResponse> result);
    Observable<Response<ProfileResponse>> getProfile(String authToken);
}
