package co.idyllic.drop.patient.view.activities;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.Gson;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.idyllic.drop.patient.Constants;
import co.idyllic.drop.patient.R;
import co.idyllic.drop.patient.dagger.DaggerInjecter;
import co.idyllic.drop.patient.model.api.EditProfile;
import co.idyllic.drop.patient.model.pojo.EditProfileRequest;
import co.idyllic.drop.patient.model.pojo.LoginResponse;
import co.idyllic.drop.patient.model.pojo.ProfileRequest;
import co.idyllic.drop.patient.model.pojo.ProfileResponse;
import co.idyllic.drop.patient.model.pojo.RequestSuccess;
import co.idyllic.drop.patient.model.pojo.User;
import co.idyllic.drop.patient.model.pojo.UserProfile;
import co.idyllic.drop.patient.presenter.EditProfilePresenter;
import co.idyllic.drop.patient.view.viewinterfaces.EditProfileView;
import io.paperdb.Paper;
import rx.Observable;

public class EditProfileActivity extends BaseActivity implements EditProfileView {
    @BindView(R.id.fulname_editText)
    EditText fullnameEditText;
    @BindView(R.id.email_editText)
    EditText emailEditText;
    @BindView(R.id.phone_editText)
    EditText phoneEditText;
    @BindView(R.id.submit_btn)
    Button doneButton;
    Context mContext;
    UserProfile mUser;
    @Inject
    EditProfile mEditProfileApi;
    EditProfilePresenter mEditDetailPresenter;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        mContext = this;
        ButterKnife.bind(this);
        DaggerInjecter.getApiComponent().inject(this);
        mEditDetailPresenter = new EditProfilePresenter();
        mEditDetailPresenter.viewInterface = EditProfileActivity.this;
        mUser = Paper.book().read(Constants.KEY_USER_PROFILE);
        initViews();
        populateData();
    }

    private void initViews() {
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mUser.setEmail(emailEditText.getText().toString());
                mUser.setPhoneNumber(phoneEditText.getText().toString());
                mUser.setName(fullnameEditText.getText().toString());
                EditProfileRequest request = new EditProfileRequest();
                request.setPatient(mUser);
                mEditDetailPresenter.editProfile(request);
            }
        });
    }

    private void populateData() {
        emailEditText.setText(mUser.getEmail());
        phoneEditText.setText(mUser.getPhoneNumber());
        fullnameEditText.setText(mUser.getName());
    }

    @Override
    public void onEditComplete() {

    }

    @Override
    public void onEditError(String message) {
        showToast("Could not reach servers");
    }

    @Override
    public void onEditSuccess(ProfileResponse result) {
        if(result.getSuccess()){
            Paper.book().write(Constants.KEY_USER_PROFILE, mUser);
            showToast("Profile edited successfully");
        } else {
            showToast("Profile not edited");
        }
    }

    @Override
    public Observable<ProfileResponse> editProfile(EditProfileRequest editProfile) {
        String authToken = Paper.book().read(Constants.KEY_AUTH_TOKEN);
        return mEditProfileApi.editUserProfile(authToken, editProfile);
    }
}
