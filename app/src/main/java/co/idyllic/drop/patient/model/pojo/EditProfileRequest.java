package co.idyllic.drop.patient.model.pojo;

/**
 * Created by Vishal on 20-09-2017.
 */



        import com.google.gson.annotations.Expose;
        import com.google.gson.annotations.SerializedName;

public class EditProfileRequest {

    @SerializedName("patient")
    @Expose
    private UserProfile patient;

    public UserProfile getPatient() {
        return patient;
    }

    public void setPatient(UserProfile patient) {
        this.patient = patient;
    }
}

