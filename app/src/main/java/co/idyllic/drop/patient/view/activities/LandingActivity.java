package co.idyllic.drop.patient.view.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.idyllic.drop.patient.R;
import co.idyllic.drop.patient.dagger.DaggerInjecter;
import co.idyllic.drop.patient.model.api.LoginService;
import co.idyllic.drop.patient.model.pojo.Login;
import co.idyllic.drop.patient.model.pojo.LoginResponse;
import co.idyllic.drop.patient.presenter.LoginPresenter;
import co.idyllic.drop.patient.view.viewinterfaces.LoginView;
import io.paperdb.Paper;
import rx.Observable;

/**
 * Created by Vishal on 06-09-2017.
 */

public class LandingActivity extends BaseActivity implements LoginView {
    Context mContext;
    @BindView(R.id.get_started_btn)
    Button getStartedBtn;
    @BindView(R.id.signin_btn_1)
    Button signinBtnFirst;
    @BindView(R.id.get_started_signin_layout)
    LinearLayout getStartedSigninLayout;
    @BindView(R.id.signin_layout)
    LinearLayout signinLayout;
    @BindView(R.id.signup_textview)
    TextView signupTextView;
    @BindView(R.id.forgot_pwd_textview)
    TextView forgotPwdTextView;
    @BindView(R.id.login_phone_editText)
    EditText phoneEditText;
    @BindView(R.id.password_edittext)
    EditText passwordEditText;
    @BindView(R.id.signin_btn_2) Button signinButtonSecond;

    @Inject
    LoginService mLoginApi;

    LoginPresenter mLoginPresenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);
        ButterKnife.bind(this);
        mContext = this;
        Paper.init(this);
        DaggerInjecter.getApiComponent().inject(this);
        init();
        if(getIntent().getBooleanExtra("signin", false)){
            getStartedSigninLayout.setVisibility(View.GONE);
            signinLayout.setVisibility(View.VISIBLE);
        }
    }

    private void init() {
        mLoginPresenter = new LoginPresenter();
        mLoginPresenter.loginView = this;
        getStartedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, SignupActivity.class);
                startActivity(intent);
            }
        });
        signinBtnFirst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getStartedSigninLayout.setVisibility(View.GONE);
                signinLayout.setVisibility(View.VISIBLE);
            }
        });
        signinButtonSecond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mLoginPresenter.login(new Login(phoneEditText.getText().toString(),
                        passwordEditText.getText().toString()));
            }
        });
        signupTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, SignupActivity.class);
                startActivity(intent);
            }
        });
        forgotPwdTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, ForgotPasswordActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onLoginComplete() {

    }

    @Override
    public void onLoginError(String message) {
        showToast("Could not log in");
    }

    @Override
    public void onLoginSuccess(LoginResponse result) {
        if(result.getData() != null) {
            String authToken = result.getData();
            Paper.book().write("auth_token", authToken);
            Intent intent = new Intent(mContext, MainActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public Observable<LoginResponse> login(Login login) {
        return mLoginApi.login(login);
    }
}
