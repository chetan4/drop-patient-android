package co.idyllic.drop.patient.model.api;

import co.idyllic.drop.patient.model.pojo.Login;
import co.idyllic.drop.patient.model.pojo.LoginResponse;
import retrofit2.http.Body;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by Vishal on 08-09-2017.
 */

public interface LoginService {
    @POST("/login")
    Observable<LoginResponse> login(@Body Login login);
}
