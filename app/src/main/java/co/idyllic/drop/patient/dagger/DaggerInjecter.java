package co.idyllic.drop.patient.dagger;

import co.idyllic.drop.patient.Constants;
import co.idyllic.drop.patient.dagger.components.ApiComponent;
import co.idyllic.drop.patient.dagger.components.DaggerApiComponent;
import co.idyllic.drop.patient.dagger.components.DaggerNetworkComponent;
import co.idyllic.drop.patient.dagger.components.NetworkComponent;
import co.idyllic.drop.patient.dagger.modules.ApiModule;
import co.idyllic.drop.patient.dagger.modules.NetworkModule;

/**
 * Created by Vishal on 08-09-2017.
 */

public class DaggerInjecter {
    private static ApiComponent appComponent = DaggerApiComponent.builder().networkComponent(
            getNetworkComponent()).build();
    public static ApiComponent getApiComponent() {
        return appComponent;
    }

    public static NetworkComponent getNetworkComponent(){
        return DaggerNetworkComponent.builder().networkModule(new NetworkModule(Constants.BASE_URL)).build();
    }
}
