package co.idyllic.drop.patient.model.pojo;

/**
 * Created by Vishal on 15-09-2017.
 */

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Doctor implements Parcelable {

    @SerializedName("city")
    @Expose
    private City city;
    @SerializedName("img")
    @Expose
    private String img;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("doctor_specialisations")
    @Expose
    private List<DoctorSpecialisation> doctorSpecialisations = null;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("phone")
    @Expose
    private Phone phone;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("doctor_qualifications")
    @Expose
    private List<DoctorQualification> doctorQualifications = null;
    @SerializedName("username")
    @Expose
    private String username;

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<DoctorSpecialisation> getDoctorSpecialisations() {
        return doctorSpecialisations;
    }

    public void setDoctorSpecialisations(List<DoctorSpecialisation> doctorSpecialisations) {
        this.doctorSpecialisations = doctorSpecialisations;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Phone getPhone() {
        return phone;
    }

    public void setPhone(Phone phone) {
        this.phone = phone;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public List<DoctorQualification> getDoctorQualifications() {
        return doctorQualifications;
    }

    public void setDoctorQualifications(List<DoctorQualification> doctorQualifications) {
        this.doctorQualifications = doctorQualifications;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    private class City {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }

    private class DoctorQualification {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }

    private class Phone {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("number")
        @Expose
        private String number;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

    }

    private class DoctorSpecialisation {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }


    protected Doctor(Parcel in) {
        city = (City) in.readValue(City.class.getClassLoader());
        img = in.readString();
        lastName = in.readString();
        id = in.readByte() == 0x00 ? null : in.readInt();
        email = in.readString();
        if (in.readByte() == 0x01) {
            doctorSpecialisations = new ArrayList<DoctorSpecialisation>();
            in.readList(doctorSpecialisations, DoctorSpecialisation.class.getClassLoader());
        } else {
            doctorSpecialisations = null;
        }
        firstName = in.readString();
        phone = (Phone) in.readValue(Phone.class.getClassLoader());
        gender = in.readString();
        if (in.readByte() == 0x01) {
            doctorQualifications = new ArrayList<DoctorQualification>();
            in.readList(doctorQualifications, DoctorQualification.class.getClassLoader());
        } else {
            doctorQualifications = null;
        }
        username = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(city);
        dest.writeString(img);
        dest.writeString(lastName);
        if (id == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(id);
        }
        dest.writeString(email);
        if (doctorSpecialisations == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(doctorSpecialisations);
        }
        dest.writeString(firstName);
        dest.writeValue(phone);
        dest.writeString(gender);
        if (doctorQualifications == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(doctorQualifications);
        }
        dest.writeString(username);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Doctor> CREATOR = new Parcelable.Creator<Doctor>() {
        @Override
        public Doctor createFromParcel(Parcel in) {
            return new Doctor(in);
        }

        @Override
        public Doctor[] newArray(int size) {
            return new Doctor[size];
        }
    };
}