package co.idyllic.drop.patient.model.pojo;

/**
 * Created by Vishal on 08-09-2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Login {

    public Login(String phone, String password){
        patient.password = password;
        patient.phoneNumber = phone;
    }

    @SerializedName("patient")
    @Expose
    private Patient patient = new Patient();

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }


    public class Patient {

        @SerializedName("phone_number")
        @Expose
        private String phoneNumber;
        @SerializedName("password")
        @Expose
        private String password;

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

    }

}