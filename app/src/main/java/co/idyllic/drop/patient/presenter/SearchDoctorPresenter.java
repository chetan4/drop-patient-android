package co.idyllic.drop.patient.presenter;

import co.idyllic.drop.patient.model.pojo.SearchDoctorRequestBody;
import co.idyllic.drop.patient.model.pojo.SearchDoctorResponse;
import co.idyllic.drop.patient.view.viewinterfaces.SearchDoctorView;
import rx.Observer;

/**
 * Created by Vishal on 15-09-2017.
 */

public class SearchDoctorPresenter extends BasePresenter implements Observer<SearchDoctorResponse> {
    public SearchDoctorView viewInterface;
    @Override
    public void onCompleted() {
        viewInterface.onSearchComplete();
    }

    @Override
    public void onError(Throwable e) {
        viewInterface.onSearchError(e.getMessage());
    }

    @Override
    public void onNext(SearchDoctorResponse searchDoctorResponse) {
        viewInterface.onSearchSuccess(searchDoctorResponse);
    }
    public void search(String request){
        unSubscribeAll();
        subscribe(viewInterface.search(request), SearchDoctorPresenter.this);
    }
}
