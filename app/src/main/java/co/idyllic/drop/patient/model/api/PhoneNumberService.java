package co.idyllic.drop.patient.model.api;

import co.idyllic.drop.patient.model.pojo.Login;
import co.idyllic.drop.patient.model.pojo.LoginResponse;
import co.idyllic.drop.patient.model.pojo.RequestSuccess;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Vishal on 13-09-2017.
 */

public interface PhoneNumberService {
    @GET("/forgot_patients_password")
    Observable<RequestSuccess> submitPhoneNumber(@Query("phone_number") String phone);
}
