package co.idyllic.drop.patient.model.api;

import co.idyllic.drop.patient.model.pojo.EditProfileRequest;
import co.idyllic.drop.patient.model.pojo.ProfileResponse;
import co.idyllic.drop.patient.model.pojo.UserProfile;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.PUT;
import rx.Observable;

/**
 * Created by Vishal on 20-09-2017.
 */

public interface EditProfile {
    @PUT("/api/patients/v1/profiles")
    Observable<ProfileResponse> editUserProfile(@Header("Authorization") String token,
                                                          @Body EditProfileRequest profile);
}
