package co.idyllic.drop.patient.model;

import com.google.gson.JsonObject;

import retrofit2.http.GET;
import retrofit2.http.Url;
import rx.Observable;

/**
 * Created by Vishal on 08-09-2017.
 */

public interface RxApiHelper {
    @GET
    Observable<JsonObject> makeRequest(@Url String url);
}
