package co.idyllic.drop.patient.presenter;

import co.idyllic.drop.patient.model.pojo.ProfileResponse;
import co.idyllic.drop.patient.view.viewinterfaces.UserProfileView;
import okhttp3.ResponseBody;
import retrofit2.HttpException;
import retrofit2.Response;
import rx.Observer;

/**
 * Created by Vishal on 19-09-2017.
 */

public class UserProfilePresenter extends BasePresenter implements Observer<Response<ProfileResponse>> {
    public UserProfileView viewInterface;
    @Override
    public void onCompleted() {
        viewInterface.onGetProfileComplete();
    }

    @Override
    public void onError(Throwable e) {
        ResponseBody body = ((HttpException) e).response().errorBody();
        System.out.print("");
        viewInterface.onGetProfileErrorError(e.getMessage());
    }

    @Override
    public void onNext(Response<ProfileResponse> profileResponse) {
        viewInterface.onGetProfileSuccess(profileResponse);
    }

    public void getProfile(String authToken){
        unSubscribeAll();
        subscribe(viewInterface.getProfile(authToken), UserProfilePresenter.this);
    }
}
