package co.idyllic.drop.patient.model.pojo;

/**
 * Created by Vishal on 08-09-2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfileRequest {

    @SerializedName("patient")
    @Expose
    private Patient patient;

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public ProfileRequest(String fullName, String phone, String email, String password){
        this.patient = new Patient();
        this.patient.name = fullName;
        this.patient.email = email;
        this.patient.phoneNumber = phone;
        this.patient.password = password;
        this.patient.img = "";
    }

    public class Patient {

        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("phone_number")
        @Expose
        private String phoneNumber;
        @SerializedName("password")
        @Expose
        private String password;
        @SerializedName("img")
        @Expose
        private String img;
        @SerializedName("name")
        @Expose
        private String name;


        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getName() {
            return name;
        }

        public void setFirstName(String name) {
            this.name = name;
        }

    }

}