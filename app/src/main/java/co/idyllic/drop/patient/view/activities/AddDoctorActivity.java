package co.idyllic.drop.patient.view.activities;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.idyllic.drop.patient.Constants;
import co.idyllic.drop.patient.R;
import co.idyllic.drop.patient.dagger.DaggerInjecter;
import co.idyllic.drop.patient.model.api.AddDoctorService;
import co.idyllic.drop.patient.model.pojo.AddDoctorRequestBody;
import co.idyllic.drop.patient.model.pojo.AddDoctorResponse;
import co.idyllic.drop.patient.model.pojo.Doctor;
import co.idyllic.drop.patient.presenter.AddDoctorPresenter;
import co.idyllic.drop.patient.view.viewinterfaces.AddDoctorView;
import io.paperdb.Paper;
import rx.Observable;

public class AddDoctorActivity extends BaseActivity implements AddDoctorView {
    @BindView(R.id.add_doctor_btn)
    Button addButton;
    @BindView(R.id.doctor_name_textView)
    TextView doctorNameTextView;
    Context mContext;
    Doctor mDoctor;
    String mDoctorCode;
    @Inject
    AddDoctorService mAddDoctorApi;
    AddDoctorPresenter mAddDoctorPresenter;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_doctor);
        mContext = this;
        ButterKnife.bind(this);
        Paper.init(this);
        DaggerInjecter.getApiComponent().inject(this);
        mAddDoctorPresenter = new AddDoctorPresenter();
        mAddDoctorPresenter.viewInterface = AddDoctorActivity.this;
        mDoctor = new Gson().fromJson(getIntent().getStringExtra("doctor"), Doctor.class);
        mDoctorCode = getIntent().getStringExtra("doctor_code");
        initViews();
    }

    private void initViews() {
        doctorNameTextView.setText(mDoctor.getFirstName() + "" + mDoctor.getLastName());
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddDoctorRequestBody body = new AddDoctorRequestBody();
                body.setUsername(mDoctorCode);
                body.setDeviceType("android");
                mAddDoctorPresenter.addDoctor(body);
            }
        });
    }

    @Override
    public void onAddComplete() {

    }

    @Override
    public void onAddError(String message) {
        showToast("Could not reach servers");
    }

    @Override
    public void onAddSuccess(AddDoctorResponse result) {
        if(result.getSuccess()){
            showToast("Doctor successfully added");
        } else {
            showToast("Doctor already added");
        }
    }

    @Override
    public Observable<AddDoctorResponse> addDoctor(AddDoctorRequestBody add) {
        String authToken = Paper.book().read(Constants.KEY_AUTH_TOKEN);
        return mAddDoctorApi.addDoctor(authToken, add);
    }
}
