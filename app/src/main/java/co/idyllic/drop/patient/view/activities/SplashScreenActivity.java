package co.idyllic.drop.patient.view.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import co.idyllic.drop.patient.R;
import io.paperdb.Paper;

public class SplashScreenActivity extends AppCompatActivity {

    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        Paper.init(this);
        String authToken = Paper.book().read("auth_token");
        setContentView(R.layout.activity_splash_screen);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = null;
                String authToken = Paper.book().read("auth_token");
                if(authToken != null) {
                    intent = new Intent(mContext, MainActivity.class);
                    startActivity(intent);
                } else{
                    intent = new Intent(mContext, LandingActivity.class);
                    startActivity(intent);
                }
                finish();
            }
        }, 2000);
    }
}
