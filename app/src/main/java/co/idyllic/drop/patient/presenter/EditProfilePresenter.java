package co.idyllic.drop.patient.presenter;

import co.idyllic.drop.patient.model.pojo.EditProfileRequest;
import co.idyllic.drop.patient.model.pojo.ProfileResponse;
import co.idyllic.drop.patient.view.viewinterfaces.EditProfileView;
import rx.Observer;

/**
 * Created by Vishal on 20-09-2017.
 */

public class EditProfilePresenter extends BasePresenter implements Observer<ProfileResponse>{
    public EditProfileView viewInterface;
    @Override
    public void onCompleted() {
        viewInterface.onEditComplete();
    }

    @Override
    public void onError(Throwable e) {
        viewInterface.onEditError(e.getMessage());
    }

    @Override
    public void onNext(ProfileResponse profileResponse) {
        viewInterface.onEditSuccess(profileResponse);
    }

    public void editProfile(EditProfileRequest editProfileRequest){
        unSubscribeAll();
        subscribe(viewInterface.editProfile(editProfileRequest), EditProfilePresenter.this);
    }
}
