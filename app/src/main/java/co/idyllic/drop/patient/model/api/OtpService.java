package co.idyllic.drop.patient.model.api;

import co.idyllic.drop.patient.model.pojo.RequestSuccess;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Vishal on 13-09-2017.
 */

public interface OtpService {
    @GET("/verify_patients_otp")
    Observable<RequestSuccess> submitOtp(@Query("otp") String otp);
}
