package co.idyllic.drop.patient.dagger.components;

import javax.inject.Singleton;

import co.idyllic.drop.patient.dagger.CustomScope;
import co.idyllic.drop.patient.dagger.modules.ApiModule;
import co.idyllic.drop.patient.dagger.modules.NetworkModule;
import co.idyllic.drop.patient.view.activities.AddDoctorActivity;
import co.idyllic.drop.patient.view.activities.EditProfileActivity;
import co.idyllic.drop.patient.view.activities.ForgotPasswordActivity;
import co.idyllic.drop.patient.view.activities.LandingActivity;
import co.idyllic.drop.patient.view.activities.MainActivity;
import co.idyllic.drop.patient.view.activities.SearchDoctorActivity;
import co.idyllic.drop.patient.view.activities.SignupActivity;
import dagger.Component;

/**
 * Created by Vishal on 08-09-2017.
 */
@Component(modules = ApiModule.class, dependencies = NetworkComponent.class)
@CustomScope
public interface ApiComponent {
    void inject(MainActivity activity);

    void inject(LandingActivity landingActivity);

    void inject(SignupActivity signupActivity);

    void inject(ForgotPasswordActivity forgotPasswordActivity);

    void inject(SearchDoctorActivity searchDoctorActivity);

    void inject(AddDoctorActivity addDoctorActivity);

    void inject(EditProfileActivity editProfileActivity);
}
