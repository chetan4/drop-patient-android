package co.idyllic.drop.patient.presenter;

import co.idyllic.drop.patient.model.pojo.RequestSuccess;
import co.idyllic.drop.patient.view.viewinterfaces.SubmitPhoneNumberView;
import rx.Observable;
import rx.Observer;

/**
 * Created by Vishal on 13-09-2017.
 */

public class SubmitPhoneNumberPresenter extends BasePresenter implements Observer<RequestSuccess> {
    public SubmitPhoneNumberView viewInterface;
    @Override
    public void onCompleted() {
        viewInterface.onPhoneNumberSubmitComplete();
    }

    @Override
    public void onError(Throwable e) {
        viewInterface.onPhoneNumberSubmitError(e.getMessage());
    }

    @Override
    public void onNext(RequestSuccess requestSuccess) {
        viewInterface.onPhoneNumberSubmitSuccess(requestSuccess);
    }

    public void submitPhoneNumber(String phone){
        unSubscribeAll();
        subscribe(viewInterface.submitPhoneNumber(phone), SubmitPhoneNumberPresenter.this);
    }
}
